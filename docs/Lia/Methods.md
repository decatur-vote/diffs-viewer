<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Global Liaison Methods  
Below is a list of `$lia->methods()` grouped by the classes those methods map to.   
  
## Global functions  
These functions are defined globally and can be called from anywhere. They can be called through liaison, as it is on the left.  
  
## Anonymous functions  
These are anonymous functions, added to Liaison.   
- `ready(...)` - Calls anonymous function, defined in [/vendor/taeluf/liaison/code/addon/Hook.php](/vendor/taeluf/liaison/code/addon/Hook.php)    
  
  
## Lia\Addon\Autoload  
no description    
- `autoload($dir, ...$baseNamespaces)` - add a directory to be autoloaded, psr4-ish. See `$autoload->addDir()`    
  
## Lia\Addon\Cache  
    
- `cache($key, $value)` - no description. See `$cache->set()`    
- `cache_get($key)` - . See `$cache->get()`    
- `cache_write()` - Write main key=>value pairs to disk from memory. See `$cache->write()`    
- `cache_read()` - reads main key=>value pairs from disk into memory. See `$cache->read()`    
- `cache_file($key, $value, $maxAge=60*60*24*5)` - Store $value to file by identified by $key. See `$cache->cache_file()`    
- `cache_get_file($key)` - Get content of a cached file, if cache is valid. See `$cache->get_cache_file_content()`    
  
## Lia\Addon\Error  
How will this be used?  
Show error at top of current page  
Redirect to error page with (or without) a message  
Show error page instead of requested page (no redirect)  
Log an error    
- `error_header(string $message, int $statusCode=null)` - no description. See `$error->error_header()`    
- `error_page(string $message, int $statusCode=500)` - no description. See `$error->error_page()`    
- `error_goto(string $message, int $statusCode=500)` - . See `$error->error_goto()`    
  
## Lia\Addon\Hook  
    
- `hook($name, $callable)` - . See `$hook->add()`    
- `call_hook($name, ...$args)` - . See `$hook->call()`    
  
## Lia\Addon\Redirect  
    
- `goto($url, $statusCode=302)` - no description. See `$redirect->goto()`    
- `gotoSetMessage($message)` - no description. See `$redirect->gotoSetMessage()`    
- `gotoWithMessage($url, $message, $statusCode=302)` - no description. See `$redirect->gotoWithMessage()`    
- `getGotoMessage()` - Get the redirect message associated with `$_GET['message_id']`. See `$redirect->getGotoMessage()`    
  
## Lia\Addon\ResourceSorter  
no description    
- `setResourceOrder($ext, $names, $prepend=true)` - Give an array of file names in the order they should be sent to the browser. See `$resourceSorter->setResourceOrder()`    
  
## Lia\Addon\Resources  
A component for managing multiple css & js files for a single request.    
- `setResourceSorter($ext, $sorter)` - no description. See `$resources->setSorter()`    
- `addResourceFile($file)` - no description. See `$resources->addFile()`    
- `addResourceUrl($url)` - no description. See `$resources->addUrl()`    
- `getResourceHtml()` - no description. See `$resources->getHtml()`    
- `getHeadHtml()` - no description. See `$resources->getHtml()`    
  
## Lia\Addon\Router  
    
- `addRoute($pattern, $callbackOrFile, $package=null)` - Add a route. See `$router->addRoute()`    
- `route(\Lia\Obj\Request $request)` - get a route for the given request. See `$router->route()`    
- `clean_url($dirty_url)` - Clean a url (without domain): replace space with '+', single quote with '%27', and replace multiple slashes with a single slash. See `$router->clean_url()`    
  
## Lia\Addon\Seo  
Resources for needed seo data/  
https://developer.twitter.com/en/docs/twitter-for-websites/cards/guides/getting-started  
https://ogp.me/  
https://www.w3schools.com/tags/tag_meta.asp  
NOT USED https://schema.org    
- `seo($params)` - Shorthand to set seo paramaters from an array.. See `$seo->seo()`    
- `seoTitle($title)` - no description. See `$seo->title()`    
- `seoDescription($description)` - . See `$seo->description()`    
- `seoImage($url, $altText)` - . See `$seo->image()`    
- `seoUrl($url)` - no description. See `$seo->url()`    
- `seoMeta(string $name, string $content)` - Add any meta tag. See `$seo->meta()`    
- `seoSiteName($siteName)` - no description. See `$seo->site_name()`    
- `getSeoHtml()` - no description. See `$seo->get_html()`    
- `addHeadHtml($html)` - no description. See `$seo->addHeadHtml()`    
  
## Lia\Addon\Server  
A very bad integration of the Router & other addons & utility classes that makes it easy to send a response    
- `getResponse($url=null)` - . See `$server->getResponse()`    
- `deliver($url=null)` - no description. See `$server->deliver()`    
- `urlWithDomain($relativeUrl)` - . See `$server->urlWithDomain()`    
- `setTheme($name)` - Set theme to use in a web response. See `$server->setTheme()`    
  
## Lia\Addon\View  
    
- `view($name, $args=[])` - . See `$view->view()`    
- `addView($view_name, $dir)` - no description. See `$view->addView()`    
- `addViewCallable($view_name, $callable)` - no description. See `$view->addViewCallable()`    
