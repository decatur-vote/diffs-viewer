<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/src/Integration.php  
  
# class DecaturVote\DiffsViewer\Test\Integration  
  
  
  
## Constants  
  
## Properties  
- `protected \PDO $pdo;`   
  
## Methods   
- `public function __construct(\PDO $pdo)`   
- `public function getPdo(): \PDO`   
- `public function canViewAdminPages(): bool`   
- `public function get_current_text(string $uuid): string`   
- `public function can_access_diff(\DecaturVote\DiffDb\TextDiff $diff): bool`   
- `public function clean_diff_opps(string $diff_ops): string`   
- `public function clean_text(string $text): string`   
- `public function diffs_will_display(\Lia $lia): void`   
  
