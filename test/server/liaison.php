<?php

require_once(__DIR__.'/../../vendor/autoload.php');
require_once(__DIR__.'/../src/Integration.php');

$lia = new \Lia();
if (!isset($pdo))$pdo = require(__DIR__.'/pdo.php');

// $lia->set('lia:server.router.varDelim', );

$server_app = new \Lia\Package\Server($lia, 'lia:server'); // second arg is fully qualified name. third arg is optional app dir. fourth is optional base url  


$lia->router->varDelim = '\\.\\/\\:';

$viewer_app = new \Lia\Package\Server($lia, 'decatur-vote:diffs-viewer', dirname(__DIR__,2).'/src/');  
$viewer_app->integration = new \DecaturVote\DiffsViewer\Test\Integration($pdo);

