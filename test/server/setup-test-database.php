<?php
/**
 * @var \PDO $pdo must be made available to this script
 */

$diffDb = new \DecaturVote\DiffDb($pdo);

$diffDb->delete('text_diffs',[]);

$diffDb->store_diff('', 'Line one', 1);
$diffDb->store_diff('Line one', "Line one\nLine two", 1); 
$diffDb->store_diff("Line one\nLine two", "Line one\nLine two\nLine three", 1); 


$diffDb->store_diff("Apple\nBanana", "Apple\nBanana\nPear", 2);

$diffDb->store_diff("", "Bear", 3);
$diffDb->store_diff("Bear", "Bear\nCat\nDog", 3);
$diffDb->store_diff("Bear\nCat\nDog", "Bear\nDog", 3);
