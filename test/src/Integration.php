<?php

namespace DecaturVote\DiffsViewer\Test;


class Integration implements \DecaturVote\DiffsViewer\IntegrationInterface {

    protected \PDO $pdo; 

    public function __construct(\PDO $pdo){
        $this->pdo = $pdo;
    }

    public function getPdo(): \PDO{
        return $this->pdo;
    }

    public function canViewAdminPages(): bool {
        return true;
    }

    public function get_current_text(string $uuid): string {
        return file_get_contents(__DIR__.'/../input/text-'.$uuid.'.txt');
    }

    public function can_access_diff(\DecaturVote\DiffDb\TextDiff $diff): bool {
        return true;
    }

    public function clean_diff_opps(string $diff_ops): string {
        return $diff_ops;
    }

    public function clean_text(string $text): string{
        return str_replace("\n", "\n<br>",$text);
    }

    public function diffs_will_display(\Lia $lia): void {
        // $lia->addResourceFile('path/to/styles.css');
    }
}
