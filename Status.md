# Status
Notes taken during & for development purposes

## May 30, 2023
Setup the viewer. Add an IntegrationInterface. All tests are server-based & are manual.

## May 26, 2023
initialize repo, get composer dependencies right. Start docs & tests. VERY early version of the diffs viewer, which currently is just a single route. Integration requires a lia method getPdo().

Next is to basically clean it up, add a cleaner integration method, make the view actually nice, and start integrating it into decaturvote.com so I can see what it's like in production.

I would like to have a diffs-html feature, but I may delay that for a future date. Right now, simply listing the diffs (or the different versions of the text) is probably enough.

I will need to be careful about security - ensuring that I prevent any XSS attacks or whatever.
