<?php
/**
 * View diffs for a UUID
 */

if (!$package->integration->canViewAdminPages()){
    header("Page Not Found", true, 404 );
    echo '<h1>Page Not Found</h1>';
    return;
}

$pdo = $package->integration->getPdo();
$diffDb = new \DecaturVote\DiffDb($pdo);

$rows = $diffDb->sql_query(
    "SELECT `source_uuid` as `uuid`, COUNT(*) as num_diffs, MAX(`diff_time`) as `last_diff_time` 
        FROM `text_diffs`
        GROUP BY `uuid`
    "
);
?>
<h1>Available Diffs</h1>
<table>
    <thead>
        <tr>
            <th>Uuid</th>
            <th>Diffs count</th>
            <th>Last Diff</th>
        </tr>
    </thead>
    <tbody>
<?php foreach ($rows as $r): ?>
        <tr>
            <td><a href="../<?=$r['uuid']?>"><?=$r['uuid']?></a></td>
            <td><?=$r['num_diffs']?></td>
            <td><?=$r['last_diff_time']?></td>
        </tr>
<?php endforeach ?>
    </tbody>
</table>
