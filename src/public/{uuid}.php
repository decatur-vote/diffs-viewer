<?php
/**
 * View diffs for a UUID
 */

$lia->addResourceFile(__DIR__.'/diffs.css');
$package->integration->diffs_will_display($lia);

$uuid = htmlspecialchars(strip_tags($uuid));

$pdo = $package->integration->getPdo();
$diffDb = new \DecaturVote\DiffDb($pdo);

$diffs = $diffDb->get_diffs($uuid);

if (count($diffs)==0){
    echo "<h1>No Diffs Found</h1>";
    echo "<p>The uuid '$uuid' has no diffs stored.</p>";
    return;
}

$current_text = $package->integration->get_current_text($uuid);

$original = $diffDb->backward($current_text, $diffs);

echo "<section class=\"DiffList\">";

if (!$package->integration->can_access_diff($diffs[0])){
    echo "<h1>Text History not available</h1>";
    echo "<p>Text change history is not available for this item.</p>";
    return;
}

echo "<h1>Text History</h1>";
echo "<h2>Original</h2>";
if (trim($original) == '') echo "<div class=\"full_text\">EMPTY</div>"; 
else echo "<div class=\"full_text\"><details><summary>View Text</summary>".$package->integration->clean_text($original)."</details></div>";
$text = $original;
foreach ($diffs as $index=>$d){
    if (!$package->integration->can_access_diff($d)){
        echo "<p>You do not have permission to view more diffs.</p>";
        break;
    }
    echo "<hr>";
    $text = $diffDb->forward($text, [$d]);
    echo "<h2>Changes on ".$d->diff_time->format("M d, Y \a\\t h:i a")."</h2>";
    echo "<div class=\"changes\">".$package->integration->clean_diff_opps($d->diff)."</div>";
    echo "<h2>New Text</h2>";
    echo "<div class=\"full_text\"><details><summary>View Text</summary>".$package->integration->clean_text($text)."</details></div>";
}

echo "</section>";
