<?php

namespace DecaturVote\DiffsViewer;

interface IntegrationInterface {
    /** get a valid PDO instance */
    public function getPdo(): \PDO;
    /** Return `true` to allow viewing admin pages */
    public function canViewAdminPages(): bool;
    /** Get the current (most recent) text for a source uuid */
    public function get_current_text(string $uuid): string; 
    /** return true to allow access to a diff. false otherwise */
    public function can_access_diff(\DecaturVote\DiffDb\TextDiff $diff): bool;
    /** return string that should be sent to the browser */
    public function clean_diff_opps(string $diff_ops): string;
    /** return string that should be sent to the browser for any version of the text */
    public function clean_text(string $text): string;
    /** Called before diffs are displayed. Use this to add styles */
    public function diffs_will_display(\Lia $lia): void;

}
