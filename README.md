<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Diffs Viewer  
This is the Diffs viewer for DecaturVote.com. Built with [Liaison](https://www.taeluf.com/docs/Liaison). Diffs are managed by [decatur-vote/diffs-db](https://gitlab.com/decatur-vote/diffsdb) and [decatur-vote/text-diff](https://gitlab.com/decatur-vote/text-diff).  
  
## Install  
```bash  
composer require decatur-vote/diffs-viewer v1.0.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "decatur-vote/diffs-viewer": "v1.0.x-dev"}}  
```  
  
  
## Routes  
The default base url is `/diff/`. (*I think `Package::base_url` can be changed, but I'm not sure how, because Liaison doesn't document it.*)  
- `/diff/{uuid}/` - display diffs for a particular source uuid. If `$integration->can_access_diff($diff)` returns false, no further diffs  or full texts will be displayed. If the first diff cannot be accessed, then the original text will not display either.  
- `/diff/admin/` - view a list of avaialble diffs (grouped by source uuid). Only viewable if `$integration->canViewAdminPages()` returns `true`  
- `/diff/diffs.css` - Base css for viewing diffs. This is automatically added when requesting `/diff/{uuid}/`.  
  
## Usage  
To setup the viewer: Set it up with Liaison, style it, and add an Integration class for needed callbacks.   
  
**NOTICE:** Setup [decatur-vote/diffs-db](https://gitlab.com/decatur-vote/diffsdb) to actually store & manage your diffs, so they can be viewed.  
  
### Setup with Liaison  
```php  
<?php  
$viewer_app = new \Lia\Package\Server($lia, 'decatur-vote:diffs-viewer', dirname(__DIR__).'/src/');    
$viewer_app->integration = new \MyIntegrationClass();  
```  
  
### Setup Integration  
Define `MyIntegrationClass`. It must implement [src/IntegrationInterface.php](/src/IntegrationInterface.php): (*This is a barebones version from our tests*)  
```php  
<?php  
  
namespace DecaturVote\DiffsViewer\Test;  
  
  
class Integration implements \DecaturVote\DiffsViewer\IntegrationInterface {  
  
    protected \PDO $pdo;   
  
    public function __construct(\PDO $pdo){  
        $this->pdo = $pdo;  
    }  
  
    public function getPdo(): \PDO{  
        return $this->pdo;  
    }  
  
    public function canViewAdminPages(): bool {  
        return true;  
    }  
  
    public function get_current_text(string $uuid): string {  
        return file_get_contents(__DIR__.'/../input/text-'.$uuid.'.txt');  
    }  
  
    public function can_access_diff(\DecaturVote\DiffDb\TextDiff $diff): bool {  
        return true;  
    }  
  
    public function clean_diff_opps(string $diff_ops): string {  
        return $diff_ops;  
    }  
  
    public function clean_text(string $text): string{  
        return str_replace("\n", "\n<br>",$text);  
    }  
  
    public function diffs_will_display(\Lia $lia): void {  
        // $lia->addResourceFile('path/to/styles.css');  
    }  
}  
```  
  
### Styling  
The only style built-in is a `white-space:pre` for the list of diffs. See [src/public/diffs.css](/src/public/diffs.css) & override styles as you wish  
```css  
/** Container for displaying diffs */  
.DiffList {  
  
}  
  
/** the full text for the current iteration */  
.DiffList .full_text {  
  
}  
  
/** The diff operations */  
.DiffList .changes {  
    white-space: pre;  
}  
```  
  
## Testing  
### Setup Mysql test settings  
At the root of `DiffsViewer`, create the file `mysql_settings.json` and fill it in with your development environment settings:  
  
**WARNING: Running tests will delete your all text diffs from your database**  
```php  
{  
    "database": "dbname",  
    "host": "localhost",  
    "user": "user_name",  
    "password": "bad_password"  
}  
```  
  
Then you'll need to `vendor/bin/phptest server`, visit the localhost url, and go to `/diff/admin/`  
  
## Screenshots  
These are with no additional styling or page theme.  
  
`/diff/admin/`:  
![3 row table showing uuid, diff count, and last diff date/time](.media/admin-page.png "/diff/admin/")  
  
<hr>  
  
`/diff/{uuid}/`:  
![A web page showing original text, a history of different versions, and the changes to yield each version](.media/diffs.png "/diff/{uuid}/")  
